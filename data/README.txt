Folder structure for RESQMLv2:

EnergyML
  |-->data
    |-->commonv2
      |v2.0
        |-->doc         - Contains Technical Usage Guide for CommonV2
        |-->xsd_schemas - The COMMONV2 schema (XSD) files
    |-->resqmlv2
      |v2.0.1
        |-->ancillary   - Contains the property kind loader file
        |-->doc         - Contains the Business Overview, Usage Guide, Technical Reference Guide,
                          and documentation on EPC, EIP and Units of Measure
                        - Also contains the RESQMLv2 Enterprise Architect Project (EAP) file
        |-->examples    - Contains example data from Geosiris that uses EPC and HDF5
        |-->xsd_schemas - The RESQMLV2 schema (XSD) files




